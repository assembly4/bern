package bern

import (
	"crypto/rsa"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
)

//TODO change to interface to adjust public key from method instead of passing it to function
func JWTAuthMiddleware(pubKey *rsa.PublicKey) gin.HandlerFunc {
	return func(c *gin.Context) {
		authorization := c.Request.Header.Get("Authorization")
		if authorization == "" {
			c.JSON(401, &gin.H{
				"code":    401,
				"message": "Invalid Authorization header",
			})
			c.Abort()
			return
		}
		parts := strings.SplitN(authorization, " ", 2)
		if len(parts) != 2 || parts[0] != "Bearer" {
			c.JSON(401, &gin.H{
				"code":    401,
				"message": "Invalid Authorization header",
			})
			c.Abort()
			return
		}

		customClaims, err := VerifyToken(&parts[1], pubKey)
		if err != nil {
			log.Println(err)
			c.JSON(401, &gin.H{
				"code":    401,
				"message": err,
			})
			c.Abort()
			return
		}

		c.Set("user", customClaims.Email)
	}
}
