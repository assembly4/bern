package bern

import (
	"crypto/rsa"
	"fmt"
	"os"
)

func Authorized(token string, devicesUuid map[string]bool, write bool, pk *rsa.PublicKey) (map[string]bool, error) {
	var url string
	var allowedDevices = make(map[string]bool)

	// validate token and get claims
	claims, err := VerifyToken(&token, pk)
	if err != nil {
		return allowedDevices, err
	}
	user := claims.Email

	// prepare url for either write or read auth
	url = fmt.Sprintf("http://%s%s",
		os.Getenv("GENEVA_FQDN"),
		os.Getenv("DEVICES_ENDPOINT"),
	)
	if write {
		url = fmt.Sprintf("http://%s%s?addressee=%s",
			os.Getenv("GENEVA_FQDN"),
			os.Getenv("DEVICES_ENDPOINT"),
			user,
		)
	}

	devices, err := GetDevices(url, token)
	if err != nil {
		return allowedDevices, err
	}

	// verify if user is authorized to action upon devices
	for _, v := range devices {
		_, exists := devicesUuid[v.Uuid]
		if exists {
			allowedDevices[v.Uuid] = true
		}
	}

	return allowedDevices, nil
}
