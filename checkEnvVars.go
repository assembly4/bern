package bern

import (
	"log"
	"os"
)

func CheckEnvVars(variables []string) {
	var set bool

	for _, variable := range variables {
		_, set = os.LookupEnv(variable)
		if !set {
			log.Fatalf("variable not set: %s", variable)
		}
	}
}
