package bern

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type tokenResponse struct {
	Token   string `json:"access_token"`
	Message string `json:"message"`
}

func Login(url string, user string, password string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var r tokenResponse

	method := "POST"
	values := map[string]string{"email": user, "password": password}
	jsonValues, _ := json.Marshal(values)
	payload := bytes.NewBuffer(jsonValues)

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	req, err := http.NewRequestWithContext(ctx, method, url, payload)
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("error getting token: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return "", fmt.Errorf("wrong StatusCode: %d", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("error reading body: %v", err)
	}

	if err := json.Unmarshal(body, &r); err != nil {
		return "", fmt.Errorf("error unmarshalling body: %v", err)
	}

	return r.Token, nil
}
