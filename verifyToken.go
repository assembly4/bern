package bern

import (
	"crypto/rsa"
	"errors"
	"log"

	"github.com/golang-jwt/jwt"
)

type UserInfo struct {
	Email string `json:"email"`
}

type CustomClaims struct {
	*jwt.StandardClaims
	*UserInfo
}

func VerifyToken(tokenString *string, publicKey *rsa.PublicKey) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(*tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})

	if err != nil {
		log.Println(err)
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors == jwt.ValidationErrorExpired {
				return nil, errors.New("expired")
			}
		}
		return nil, errors.New("verify failed")
	}

	claims := token.Claims.(*CustomClaims)

	return claims, nil
}
