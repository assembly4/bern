package bern

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type device struct {
	Owner string `json:"owner"`
	Uuid  string `json:"uuid"`
}

type devicesResponse struct {
	Devices []device `json:"devices"`
	Message string   `json:"message"`
}

func GetDevices(url string, token string) ([]device, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var r devicesResponse

	method := "GET"

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	req, err := http.NewRequestWithContext(ctx, method, url, nil)
	if err != nil {
		return nil, err
	}

	authHeader := "Bearer" + " " + token
	req.Header.Add("Authorization", authHeader)

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("wrong StatusCode from devices endpoint: %d", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(body, &r); err != nil {
		return nil, err
	}

	return r.Devices, nil
}
