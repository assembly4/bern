package bern

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

func LoadPrivateKey(key_string string) (*rsa.PrivateKey, error) {
	privPem, _ := pem.Decode([]byte(key_string))

	privKey, err := x509.ParsePKCS1PrivateKey(privPem.Bytes)
	if err != nil {
		return nil, err
	}

	return privKey, nil
}

func LoadPublicKey(key_string string) (*rsa.PublicKey, error) {
	pubPem, _ := pem.Decode([]byte(key_string))

	pubKey, err := x509.ParsePKIXPublicKey(pubPem.Bytes)
	if err != nil {
		return nil, err
	}

	return pubKey.(*rsa.PublicKey), nil
}
